<?php
// $Id: earth_hour.module,v 1.15 2010/11/11 04:10:49 mgifford Exp $

/**
 * @file
 * earth_hour.module
 */
 
 /**  
 * Earth Hour Module migrated from word press plugin - http://www.bravenewcode.com/earth-hour/
 * This module is GPL so feel free to re-use, redistribute & remake it
 *
 * TODO 
 * - stop conflict with space for admin menu - maybe user_access('access administration menu')
 * - Check for # sites with initial install
 * - Provide message to say to set up blocks or create initial block
 * - Nix Block code and use drupal_add_js for everything
 */

/**
 * Implements hook_menu().
 */
function earth_hour_menu() {
  $items['admin/config/user-interface/earth_hour'] = array(
    'title' => 'Earth hour',
    'description' => 'The Earth Hour block allows you to show your support for Earth Hour on your site.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('earth_hour_admin_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Menu callback; presents the sitemap settings page.
 */
function earth_hour_admin_settings_form($form_state) {
  // A button to press check the effect of the results
  $form['earth_hour_test'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test Earth Hour Functionality'),
  );
  
  $form['earth_hour_test']['clear'] = array(
    '#prefix' => '<p>' . t('This is a very simple test that will not affect your whole site.') . '</p>', 
    '#type' => 'submit',
    '#value' => t('Check function'),
    '#submit' => array('check_function_submit'),
  );
  
  return $form;
}

function check_function_submit() {
  $earth_hour_minutes = 60;
  $message = t('This is just a simulation. <a href="/admin/config/earth_hour">Go back</a>.');
    earth_hour_offline($earth_hour_minutes, $message);
}

/**
 * Implementation of hook_block_info().
 */
function earth_hour_block_info() {
  // This example comes from node.module.
  $blocks['earth_hour'] = array(
    'info' => t('Earth Hour'),
    //     $blocks[0]['description'] = t('The Earth Hour block allows you to show your support for Earth Hour on your site.');
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}


/**
 * Implementation of hook_block_view().
 * add basic html with raw details about earth hour
 */
function earth_hour_block_view($delta = '') {
  $block = array();
  if ($delta == 0) {
    // Show block if Earth Hour is less than 2 months away, hasn't passed for this year
    if (5259487  > variable_get('time_until_earth_hour', 0) && variable_get('time_until_earth_hour', 0) > 0) {
      drupal_add_css(drupal_get_path('module', 'earth_hour') . '/earth_hour.css');
      $earth_hour_count = variable_get('earth_hour_count', 0);
 
      // Check for critical mass
      if ($earth_hour_count > 20) {
        $message = t('One of !count websites proudly supporting <a href="http://openconcept.ca/earth_hour">Earth Hour</a>. On Drupal? Get the <a href="http://drupal.org/project/earth_hour">module</a>',  array('!count' => $earth_hour_count));
      }
      else {
        $message = t('Proudly supporting <a href="http://openconcept.ca/earth_hour">Earth Hour</a>. On Drupal? Get the <a href="http://drupal.org/project/earth_hour">module</a>');
      }

      $earth_hour_home_page = '<a id="earth_hour_banner" href="http://www.earthhour.org" style="text-decoration:none;">' . t('Visit the Earth Hour Website') . '</a>';
      $message1 = '<div id="inner" style="color:#fff;">' . $message . '</div>'; 
      $block['content'] = "<div class=\"wrap\" id=\"earth_hour\" style=\"color:#fff;line-height:25px;\">$earth_hour_home_page $message1</div>\n" . earth_hour_dynamic_header($message1);
    }
  }
  return $block;
}


/* 
 * add javascript functionality
 */
function earth_hour_dynamic_header($message1) {
  drupal_add_js(drupal_get_path('module', 'earth_hour') . '/earth_hour.js');
  $time_until_earth_hour = variable_get('time_until_earth_hour', 0);
  $days = round($time_until_earth_hour/(24*3600), 0);
  $hours = round(($time_until_earth_hour % (24*3600))/3600, 0);
  $minutes = round((($time_until_earth_hour % (24*3600)) % 3600)/60, 0);
  $timezone = (variable_get('date_default_timezone', 0)/3600);
  $timezone_array = array(
    '10' => t('AEST'), 
    '9:30' => t('ACST'), 
    '9' => t('JST'), 
    '8' => t('AWST'), 
    '0' => t('GMT'),
    '-3:30' => t('NST'),
    '-4' => t('AT'),
    '-5' => t('ET'), 
    '-6' => t('CT'),
    '-7' => t('MT'), 
    '-8' => t('PT'),
    '-9' => t('AKT'), 
    '-10' => t('HT'),
  );
  $message2 = t('<a href="http://openconcept.ca/earth_hour/" rel="nofollow">Earth Hour</a> begins in !days days, !hours hours & !minutes minutes', array('!days' => $days, '!hours' => $hours, '!minutes' => $minutes));
  $message3 = t('@site_name will not be available for an hour 8:30pm on Saturday, March !date !timezone to show support for <a href="http://www.earthhour.org/" rel="nofollow">Earth Hour</a>', array('@site_name' => variable_get('site_name', 'Drupal'), '!date' => strftime('%e', earth_hour_find_date()), '!timezone' => $timezone_array[$timezone]));
  $output = '<script type="text/javascript">var eh_msg_1 = \'' . $message1 . '\'; var eh_msg_2 = \'' . $message2 . '\'; var eh_msg_3 = \'' . $message3 . '\';</script>';
  return $output;
}


/*
 * Call earth hour funciton on module initialization 
 */
function earth_hour_init() {
  earth_hour_check_time();
}


/* 
 * get day for earth hour for each year
 */
function earth_hour_find_date() {
  $year = date('Y');
  $last_day_of_march = date('D', strtotime(date("$year-3-31"))); 
  $days_array = array('Mon' => -2, 'Tue' => -3, 'Wed' => -4, 'Thu' => -5, 'Fri' => -6, 'Sat' => 0, 'Sun' => -1);
  $last_sat_of_march = (31 + $days_array[$last_day_of_march]);
  $earth_hour_start_time  = gmmktime(20, 30, 0, 3, $last_sat_of_march, $year);
  return $earth_hour_start_time;
}


/* 
 * turn off site for 1 hour in the right hour
 */
function earth_hour_check_time() {
  // Get timestamp for GMT date and make it perpetual
  $earth_hour_start_time = earth_hour_find_date();

  // Add hour to the Drupal start time
  $earth_hour_end_time = ($earth_hour_start_time + 60*60);

  // now time adjusted for Drupal timezone
  $adjusted_time = (REQUEST_TIME + variable_get('date_default_timezone', 0));
  
  $in_earth_hour = ($adjusted_time >= $earth_hour_start_time  && $adjusted_time <= $earth_hour_end_time);

  variable_set('time_until_earth_hour', ($earth_hour_start_time - $adjusted_time));

  if ($in_earth_hour) { // we are in earth hour
    // but let people hit the admin panel & login
    if (strpos(request_uri(), 'admin') === FALSE && strpos(request_uri(), 'user' ) === FALSE ) {
      $earth_hour_minutes = round(($earth_hour_end_time - $adjusted_time)/60, 0);
      earth_hour_offline($earth_hour_minutes);
    }
  } 
}


/* 
 * turn off site for 1 hour
 */
function earth_hour_offline($earth_hour_minutes, $message) {
  $earth_hour_count = variable_get('earth_hour_count', 0);
  echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">';
  echo '<head><title>' . variable_get('site_name', 'Drupal') . ' | ' . t('Proudly Supporting Earth Hour') . '</title>';
  echo '<link rel="stylesheet" type="text/css" media="screen" href="/' . drupal_get_path('module', 'earth_hour') . '/shutdown.css"></link>';
  echo '</head><body><div id="page"><div id="content">';
  echo '<h2>' . t('@site_name is currently participating in Earth Hour', array('@site_name' => variable_get('site_name', 'Drupal'))) . '</h2>';

  // Check for critical mass
  if ($earth_hour_count > 20) {
    echo '<p id="duane">' . t('There are currently !count other Drupal sites supporting this cause.', array('!count' => variable_get('earth_hour_count', 0))) . '</p>';
  }
  echo '<p id="chloe">' . t('If you\'d like to join us, download the <br /><a href="http://openconcept.ca/earth_hour/">Drupal Earth Hour plugin</a>') . '</p>';
  echo '<p id="dale">' . t('This website will be back online in !minutes minutes.', array('!minutes' => $earth_hour_minutes)) . '</p>';
  if (!empty($message)) { 
    echo '<h3>' . $message . '</h3>';
  }
  echo '</div></div></body></html>';

  // disable site
  die();
}


/* 
 * check number of sites & time on cron run
 */
function earth_hour_cron() {
  // Just run cron once a day
  $expires = variable_get('earth_hour_cron_last_run', REQUEST_TIME);
  
  if (($expires - REQUEST_TIME)/60/60 > 24) {
    variable_set('earth_hour_cron_last_run', REQUEST_TIME);
    earth_hour_active();
  }
}


/* 
 * check and set number of active sites
 */
function earth_hour_active() {
   $url = 'http://openconcept.ca/sites/openconcept.ca/earth_hour_count.php?site=' . md5($_SERVER['SERVER_NAME']) . '&tz=' . urlencode(variable_get('date_default_timezone', 0)); 
   $url .= '&name=' . str_replace('www.', '', $_SERVER['SERVER_NAME']);
   $r = new HTTPRequest($url);
   $value = $r->DownloadToString();
   list($site_count, $time_remaining) = split(';', $value);
   variable_set('earth_hour_count', $site_count);
}


// class to download url - probably overkill - from php.net 
class HTTPRequest {
    var $_fp;        // HTTP socket
    var $_url;        // full URL
    var $_host;        // HTTP host
    var $_protocol;    // protocol (HTTP/HTTPS)
    var $_uri;        // request URI
    var $_port;        // port
   
    // scan url
    function _scan_url() {
        $req = $this->_url;
       
        $pos = strpos($req, '://');
        $this->_protocol = strtolower(substr($req, 0, $pos));
       
        $req = substr($req, $pos+3);
        $pos = strpos($req, '/');
        if ($pos === FALSE)
            $pos = strlen($req);
        $host = substr($req, 0, $pos);
       
        if (strpos($host, ':') !== FALSE) {
            list($this->_host, $this->_port) = explode(':', $host);
        }
        else {
            $this->_host = $host;
            $this->_port = ($this->_protocol == 'https') ? 443 : 80;
        }
       
        $this->_uri = substr($req, $pos);
        if ($this->_uri == '')
            $this->_uri = '/';
    }
   
    // constructor
    function HTTPRequest($url) {
        $this->_url = $url;
        $this->_scan_url();
    }
   
    // download URL to string
    function DownloadToString() {
        $crlf = "\r\n";
       
        // generate request
        $req = 'GET ' . $this->_uri . ' HTTP/1.0' . $crlf .
        'Host: ' . $this->_host . $crlf . $crlf;
       
        // fetch
        $this->_fp = fsockopen(($this->_protocol == 'https' ? 'ssl://' : '') . $this->_host, $this->_port);
        fwrite($this->_fp, $req);
        while (is_resource($this->_fp) && $this->_fp && !feof($this->_fp))
            $response .= fread($this->_fp, 1024);
        fclose($this->_fp);
       
        // split header and body
        $pos = strpos($response, $crlf . $crlf);
        if ($pos === FALSE)
            return ($response);
        $header = substr($response, 0, $pos);
        $body = substr($response, $pos + 2 * strlen($crlf));
       
        // parse headers
        $headers = array();
        $lines = explode($crlf, $header);
        foreach ($lines as $line)
            if (($pos = strpos($line, ':')) !== FALSE)
                $headers[strtolower(trim(substr($line, 0, $pos)))] = trim(substr($line, $pos+1));
       
        // redirection?
        if (isset($headers['location'])) {
            $http = new HTTPRequest($headers['location']);
            return ($http->DownloadToString($http));
        }
        else {
            return ($body);
        }
    }
}
